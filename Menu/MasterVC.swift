//
//  MasterVC.swift
//  Menu
//
//  Created by Javier Ramon Gil on 29/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class MasterVC: UIViewController {
    var buttonTitle:String!
    var detailViewController: DetailViewController? = nil
    @IBOutlet var buttonLabels: [UIButton]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "NavBarBg"), forBarMetrics: UIBarMetrics.Default)
        view.backgroundColor = UIColor(patternImage: UIImage(named: "ContentBg")!)
        
        buttonLabels[0].setTitle(NSLocalizedString("BUTTON_ONE", comment: "Starters"), forState: .Normal)
        buttonLabels[1].setTitle(NSLocalizedString("BUTTON_TWO", comment: "Rices"), forState: .Normal)
        buttonLabels[2].setTitle(NSLocalizedString("BUTTON_THREE", comment: "Meats"), forState: .Normal)
        buttonLabels[3].setTitle(NSLocalizedString("BUTTON_FOUR", comment: "Fish"), forState: .Normal)
        buttonLabels[4].setTitle(NSLocalizedString("BUTTON_FIVE", comment: "Desserts"), forState: .Normal)

        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = controllers[controllers.count-1].topViewController as? DetailViewController
        }
      
    }

    @IBAction func buttonPressed(sender:UIButton){
        buttonTitle = sender.titleLabel!.text
        performSegueWithIdentifier("showDetail", sender: nil)
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showDetail"{
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
            controller.titleDetail = buttonTitle
        }
    }

}
