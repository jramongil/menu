//
//  DetailViewController.swift
//  Menu
//
//  Created by Javier Ramon Gil on 27/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var textView:UITextView!
    var titleDetail = ""
    var array = [String]()
    var text:String!
    var myDict:NSDictionary!
    var language:String!

    /* var detailItem: AnyObject? {
        didSet {
            // Update the view.
            //self.configureView()
        }
    }*/

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 30))
        toolbar.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        view.addSubview(toolbar)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "NavBarBg"), forBarMetrics: UIBarMetrics.Default)
        view.backgroundColor = UIColor(patternImage: UIImage(named: "ContentBg")!)
        
        titleLabel.text = titleDetail
        
        let path = NSBundle.mainBundle().pathForResource("Data", ofType: "plist")
        myDict = NSDictionary(contentsOfFile: path!)
        
        let locale = NSLocale.currentLocale()
        let currentLangID = (NSLocale.preferredLanguages() as! [String])[0]
        let displaying = locale.displayNameForKey(NSLocaleLanguageCode, value: currentLangID)
        language = displaying?.capitalizedStringWithLocale(locale)
        
        if language == "English"{
            if titleDetail == "Starters"{
                for (var i = 0; i < myDict.objectForKey("Starters")?.count(); i++){
                    array.append(myDict.objectForKey("Starters")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Rices" {
                for (var i = 0; i < myDict.objectForKey("Rices")?.count(); i++){
                    array.append(myDict.objectForKey("Rices")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Meats" {
                for (var i = 0; i < myDict.objectForKey("Meats")?.count(); i++){
                    array.append(myDict.objectForKey("Meats")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Fish" {
                for (var i = 0; i < myDict.objectForKey("Fish")?.count(); i++){
                    array.append(myDict.objectForKey("Fish")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Desserts" {
                for (var i = 0; i < myDict.objectForKey("Desserts")?.count(); i++){
                    array.append(myDict.objectForKey("Desserts")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            }
        
        } else if language == "Español"{
            
            if titleDetail == "Entrantes"{
                for (var i = 0; i < myDict.objectForKey("Entrantes")?.count(); i++){
                    array.append(myDict.objectForKey("Entrantes")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Arroces" {
                for (var i = 0; i < myDict.objectForKey("Arroces")?.count(); i++){
                    array.append(myDict.objectForKey("Arroces")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Carnes" {
                for (var i = 0; i < myDict.objectForKey("Carnes")?.count(); i++){
                    array.append(myDict.objectForKey("Carnes")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Pescados" {
                for (var i = 0; i < myDict.objectForKey("Pescados")?.count(); i++){
                    array.append(myDict.objectForKey("Pescados")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            } else if titleDetail == "Postres" {
                for (var i = 0; i < myDict.objectForKey("Postres")?.count(); i++){
                    array.append(myDict.objectForKey("Postres")?.objectAtIndex(i) as! String)
                }
                
                text = "\n".join(array)
                textView.text = text
            }
        }
        
    }



}

